<?php

namespace OOPMentor\ShippingMethod;

use OOPMentor\Order;

class Counter implements ShippingMethod
{
    public function calculatePrice(Order $order): float
    {
        return 0;
    }
}
