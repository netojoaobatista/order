<?php

namespace OOPMentor\ShippingMethod;

use OOPMentor\Order;

interface ShippingMethod
{
    public function calculatePrice(Order $order): float;
}
