<?php

namespace OOPMentor\ShippingMethod;

use OOPMentor\Order;

class Delivery implements ShippingMethod
{
    public function calculatePrice(Order $order): float
    {
        return 10;
    }
}
