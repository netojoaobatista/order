<?php

namespace OOPMentor;

/**
 * Represents a shipping address for an order, which extends the base Address class.
 * Adds a "receiver" property to capture the name of the person receiving the
 * shipment at the address. This allows shipping addresses to store additional
 * recipient details beyond the base address data.
 *
 * Inherits the address properties like zip code and number from the parent Address
 * class.
 */
class ShippingAddress extends Address
{
    /**
     * Calls the parent constructor and sets the receiver.
     *
     * @param string $receiver The receiver's name.
     * @param string $zip      The shipping address's zip code.
     * @param string $number   The shipping address's number.
     */
    public function __construct(private string $receiver, string $zip, string $number)
    {
        parent::__construct($zip, $number);
    }

    /**
     * Gets the receiver's name.
     *
     * @return string
     */
    public function getReceiver(): string
    {
        return $this->receiver;
    }

    /**
     * Sets the receiver's name.
     *
     * @param string $receiver The receiver's name.
     *
     * @return void
     */
    public function setReceiver(string $receiver): void
    {
        $this->receiver = $receiver;
    }
}
