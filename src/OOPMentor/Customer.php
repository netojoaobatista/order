<?php

namespace OOPMentor;

/**
 * Represents a customer object that can have a name, billing address, and shipping
 * address.
 *
 * The constructor accepts the customer's name, billing address, and shipping address.
 * These are stored as private properties. Getter methods are provided to access the
 * name, billing address, and shipping address properties.
 *
 * Setter methods allow updating the name, billing address, and shipping address.
 */
class Customer
{
    /**
     * Creates a new customer object.
     *
     * @param string          $name            The customer's name.
     * @param Address         $billingAddress  The customer's billing address.
     * @param ShippingAddress $shippingAddress The customer's shipping address.
     */
    public function __construct(
        private string $name,
        private Address $billingAddress,
        private ShippingAddress $shippingAddress
    ) {
    }

    /**
     * Gets the customer's name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets the customer's name.
     *
     * @param string $name The customer's name.
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Gets the customer's billing address.
     *
     * @return Address
     */
    public function getBillingAddress(): Address
    {
        return $this->billingAddress;
    }

    /**
     * Sets the customer's billing address.
     *
     * @param Address $billingAddress The customer's billing address.
     *
     * @return void
     */
    public function setBillingAddress(Address $billingAddress): void
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * Gets the customer's shipping address.
     *
     * @return ShippingAddress
     */
    public function getShippingAddress(): ShippingAddress
    {
        return $this->shippingAddress;
    }

    /**
     * Sets the customer's shipping address.
     *
     * @param ShippingAddress $shippingAddress The customer's shipping address.
     *
     * @return void
     */
    public function setShippingAddress(ShippingAddress $shippingAddress): void
    {
        $this->shippingAddress = $shippingAddress;
    }
}
