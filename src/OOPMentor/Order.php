<?php

namespace OOPMentor;

use OOPMentor\OrderState\OrderState;
use OOPMentor\OrderState\OrderStateFacade;
use OOPMentor\ShippingMethod\ShippingMethod;

/**
 * Represents an order placed by a customer.
 *
 * Methods to change the order state:
 *
 * * `complete()`: Calls the `complete()` method on the current state object.
 * * `cancel()`: Calls the `cancel()` method on the current state object.
 * * `refund()`: Calls the `refund()` method on the current state object.
 */
class Order
{
    /**
     * A kind of Singleton; we don't want to create this more than once, because
     * we don't know the cost to create one.
     *
     * @var OrderStateFacade|null
     */
    private static ?OrderStateFacade $defaultOrderStateFacade = null;

    /**
     * Current order's state.
     *
     * @var OrderState
     */
    private OrderState $state;

    /**
     * The order state facade used to avoid coupling.
     *
     * @var OrderStateFacade|null
     */
    private ?OrderStateFacade $stateFacade;

    /**
     * The shipping method applied to the order.
     *
     * @var ShippingMethod|null
     */
    private ?ShippingMethod $shippingMethod = null;

    /**
     * Order's products.
     *
     * @var Product[]
     */
    private array $items = [];

    /**
     * Creates an order and sets its initial state to `Pending`.
     *
     * @param Customer              $customer    The customer placing the order.
     * @param OrderStateFacade|null $stateFacade The order state facade used to avoid coupling.
     */
    public function __construct(private readonly Customer $customer, ?OrderStateFacade $stateFacade = null)
    {
        if ($stateFacade === null) {
            $stateFacade = $this->getDefaultOrderStateFacade();
        }

        $this->stateFacade = $stateFacade;
        $this->state       = $this->stateFacade->createPendingState();
    }

    /**
     * @param ShippingMethod $shippingMethod
     *
     * @return void
     */
    public function setShippingMethod(ShippingMethod $shippingMethod): void
    {
        $this->shippingMethod = $shippingMethod;
    }

    /**
     * Gets the subtotal of the cart, summing the total price of all items.
     *
     * @return float
     */
    public function getSubTotal(): float
    {
        $total = 0;

        foreach ($this->items as $item) {
            $total += $item->getPrice();
        }

        return $total;
    }

    /**
     * Total price of the order, including shipping.
     *
     * @return float
     */
    public function getTotal(): float
    {
        $shipping = 0;

        if ($this->shippingMethod !== null) {
            $shipping = $this->shippingMethod->calculatePrice($this);
        }

        return $this->getSubTotal() + $shipping;
    }

    /**
     * Get state facade.
     *
     * @return OrderStateFacade
     */
    public function getStateFacade(): OrderStateFacade
    {
        /**
         * @phpstan-ignore-next-line the stateFacade property is already checked and
         * defined in the constructor.
         */
        return $this->stateFacade;
    }

    /**
     * If a state facade is not defined nor provided, create a default one.
     *
     * @return OrderStateFacade
     */
    public function getDefaultOrderStateFacade(): OrderStateFacade
    {
        if (self::$defaultOrderStateFacade === null) {
            self::$defaultOrderStateFacade = new OrderStateFacade();
        }

        return self::$defaultOrderStateFacade;
    }

    /**
     * Add a product to the order.
     *
     * @param Product $product The product to add.
     *
     * @return void
     */
    public function add(Product $product): void
    {
        $this->items[] = $product;
    }

    /**
     * Count the number of products in the order.
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->items);
    }

    /**
     * Set the order's state.
     *
     * @param OrderState $state The new state.
     *
     * @return void
     */
    public function setState(OrderState $state): void
    {
        $this->state = $state;
    }

    /**
     * Gets the current order's state.
     *
     * @return OrderState
     */
    public function getState(): OrderState
    {
        return $this->state;
    }

    /**
     * Gets the order's customer.
     *
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * Complete this order.
     *
     * @return string
     */
    public function complete(): string
    {
        return $this->state->complete($this);
    }

    /**
     * Cancel this order.
     *
     * @return string
     */
    public function cancel(): string
    {
        return $this->state->cancel($this);
    }

    /**
     * Refund this order.
     *
     * @return string
     */
    public function refund(): string
    {
        return $this->state->refund($this);
    }
}
