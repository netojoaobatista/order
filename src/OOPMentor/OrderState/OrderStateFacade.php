<?php

namespace OOPMentor\OrderState;

/**
 * Acts as a facade to abstract the concrete OrderState classes. Hides the complexity
 * of state instance creation from client code. Provides factory methods to instantiate
 * each state instance type. This decouples clients from needing direct references to
 * state classes, improving loose coupling.
 *
 * Currently, supports creating the following state instances:
 *
 * * `CancelledState`
 * * `CompletedState`
 * * `PendingState`
 * * `RefundedState`
 *
 * Each factory method calls the corresponding state class constructor and returns an
 * OrderState instance. New states can be added without changing the facade interface.
 */
class OrderStateFacade
{
    /**
     * Create a cancelled state instance.
     *
     * @return OrderState
     */
    public function createCancelledState(): OrderState
    {
        return new CancelledState();
    }

    /**
     * Create a completed state instance.
     *
     * @return OrderState
     */
    public function createCompletedState(): OrderState
    {
        return new CompletedState();
    }

    /**
     * Create a pending state instance.
     *
     * @return OrderState
     */
    public function createPendingState(): OrderState
    {
        return new PendingState();
    }

    /**
     * Create a refunded state instance.
     *
     * @return OrderState
     */
    public function createRefundedState(): OrderState
    {
        return new RefundedState();
    }
}
