<?php

namespace OOPMentor\OrderState;

use OOPMentor\Order;

/**
 * Represents the "pending" state of an Order. A pending order is one that has not been
 * completed or cancelled yet. It implements the OrderState interface to define the
 * behavior of a pending order. And can be changed to a completed state or a cancelled
 * state.
 *
 * Uses the `OrderStateTrait` trait to inherit common methods.
 *
 * * The `complete()` method changes the order state to a completed state.
 * * The `cancel()` method changes the order state to a cancelled state.
 */
class PendingState implements OrderState
{
    use OrderStateTrait;

    /**
     * Changes the order state to a completed state.
     *
     * @param Order $order The order being completed.
     *
     * @return string
     */
    public function complete(Order $order): string
    {
        $order->setState($order->getStateFacade()->createCompletedState());

        return 'completed';
    }

    /**
     * Changes the order state to a cancelled state.
     *
     * @param Order $order The order being cancelled.
     *
     * @return string
     */
    public function cancel(Order $order): string
    {
        $order->setState($order->getStateFacade()->createCancelledState());

        return 'cancelled';
    }
}
