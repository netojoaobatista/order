<?php

namespace OOPMentor\OrderState;

use OOPMentor\Order;

/**
 * Defines the common behavior expected of all Order state classes.
 *
 * Any class that represents a state an Order can be in should implement this interface.
 *
 * By enforcing these common methods, any new state classes will provide a consistent
 * way to interact with Orders regardless of their current state. This helps encapsulate
 * state-specific behavior within each class while maintaining a uniform interface.
 */
interface OrderState
{
    /**
     * Called when attempting to complete an order.
     *
     * @param Order $order The order being completed.
     *
     * @return string
     */
    public function complete(Order $order): string;

    /**
     * Called when attempting to cancel an order.
     *
     * @param Order $order The order being cancelled.
     *
     * @return string
     */
    public function cancel(Order $order): string;

    /**
     * Called when attempting to refund an order.
     *
     * @param Order $order The order being refunded.
     *
     * @return string
     */
    public function refund(Order $order): string;
}
