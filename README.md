# Order management system

This project implements an order management system using object-oriented design patterns. It allows creating customers,
products, orders and managing the states of orders.

![order class diagram](order-class-diagram.svg)

## Key features

* `Customer` class to represent customer details like name, billing/shipping addresses
* `Product` class to represent items that can be added to orders
* `Order` class to manage customer orders and associated items
* `Address` and `ShippingAddress` classes for customer addresses
* `OrderState` classes to implement the State pattern for order states
* `OrderStateFacade` to abstract state object creation using Facade

## Architecture

The project follows principles of separation of concerns and high cohesion. Core classes are:

* `Customer` - Represents customer data
* `Product` - Represents order line items
* `Order` - Associates customers with products and manages state
* `Address` - Base address class for billing/shipping
* `ShippingAddress` - Extends Address for shipping specifics
* `OrderState` classes - Concrete state classes for each order status
* `OrderStateFacade` - Facade to simplify state object handling

Dependency injection is used to decouple classes. Interfaces promote loose coupling and extensibility.

## Usage

To use the system, first create a Customer with addresses. Then add Products to an Order associated with that Customer.
Call methods on Order to complete, cancel or refund it, which delegates to the current state object.

## Testing

Automated tests are included to validate class behavior. Tests cover:

* Customer and address data validation
* Product attributes
* Order lifecycle and state changes
* Facade state object creation

Tests are run using PHPUnit on Travis CI for continuous integration.

## Docker support

This project provides Docker support to help developers and CI/CD pipelines standardize the testing environment.

### Dockerfile

A Dockerfile is included which builds an image containing all dependencies to run tests and code quality checks. It can
be built with:

```shell
docker build -t order .
```

### Running tests in a container

Once built, the Docker image can be used to launch containers for testing. This ensures a consistent environment
wherever the image is run. To run tests in a container:

```shell
docker run order
```

This executes all tests, linting and checks inside the container using the image dependencies.

The Docker support helps automate testing and provides portability between different machines. Developers can also use
the image locally for testing in a reproducible way.