FROM php:8.2

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git zip unzip

RUN pecl install xdebug

ADD composer.json /app/composer.json
ADD composer.lock /app/composer.lock
ADD phpcs.xml /app/phpcs.xml
ADD phpstan.neon /app/phpstan.neon
ADD phpunit.xml /app/phpunit.xml
ADD src /app/src
ADD tests /app/tests

RUN echo "; Xdebug\nzend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20220829/xdebug.so\n\nxdebug.remote_handler=dbgp\nxdebug.start_with_request=yes" > /usr/local/etc/php/conf.d/xdebug.ini

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    php -r "unlink('composer-setup.php');" && \
    mv composer.phar /usr/bin/composer && \
    composer install --working-dir=/app

WORKDIR /app
ENTRYPOINT ["composer", "build"]