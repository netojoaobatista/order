<?php

namespace OOPMentor;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    /**
     * @return void
     */
    #[TestDox('Constructing a customer will set the name and the customer addresses')]
    public function testConstructingACustomerWillSetTheNameAndTheCustomerAddresses()
    {
        $name            = 'John';
        $zip             = '123';
        $number          = '456';
        $receiver        = 'Neto';
        $billingAddress  = new Address($zip, $number);
        $shippingAddress = new ShippingAddress($receiver, $zip, $number);
        $customer        = new Customer($name, $billingAddress, $shippingAddress);

        Assert::assertEquals($name, $customer->getName());
        Assert::assertSame($billingAddress, $customer->getBillingAddress());
        Assert::assertSame($shippingAddress, $customer->getShippingAddress());
    }

    /**
     * @return void
     */
    #[TestDox('Setting billing and shipping addresses will change the customer addresses')]
    public function testSettingBillingAndShippingAddressesWillChangeTheCustomerAddresses(): void
    {
        $billingAddress  = new Address('123456', '123');
        $shippingAddress = new ShippingAddress('Neto', '123', '456');
        $customer        = new Customer(
            'João Batista Neto',
            new Address('Rua dos bobos', '0'),
            new ShippingAddress('Neto', 'Rua dos bobos', '0')
        );

        $customer->setBillingAddress($billingAddress);
        $customer->setShippingAddress($shippingAddress);

        Assert::assertSame($billingAddress, $customer->getBillingAddress());
        Assert::assertSame($shippingAddress, $customer->getShippingAddress());
    }

    /**
     * @return void
     */
    #[TestDox('Setting name will change the customer name')]
    public function testSettingNameWillChangeTheCustomerName(): void
    {
        $customer = new Customer('John', new Address('123456', '123'), new ShippingAddress('Neto', '123', '456'));

        $customer->setName('Jane');

        Assert::assertEquals('Jane', $customer->getName());
    }
}
