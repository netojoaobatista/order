<?php

namespace OOPMentor;

use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    /**
     * @return void
     */
    #[TestDox("Setting a product's name and price retrieves the values set at creation")]
    public function testGettingAProductsNameAndPriceRetrievesTheValuesSetAtCreation(): void
    {
        $product = new Product('Produto 1', 100);

        Assert::assertEquals('Produto 1', $product->getName());
        Assert::assertEquals(100, $product->getPrice());
    }
}
