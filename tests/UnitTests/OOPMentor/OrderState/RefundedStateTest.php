<?php

namespace OOPMentor\OrderState;

use LogicException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;

class RefundedStateTest extends AbstractOrderStateTestCase
{
    /**
     * @return void
     */
    #[TestDox('Refunded order cannot be refunded again')]
    public function testRefundedOrderCannotBeRefundedAgain(): void
    {
        $order = $this->createOrder();

        $order->complete();
        $order->refund();

        Assert::assertInstanceOf(RefundedState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Already refunded');

        $order->refund();
    }

    /**
     * @return void
     */
    #[TestDox('Refunded order cannot be completed')]
    public function testRefundedOrderCannotBeCompleted(): void
    {
        $order = $this->createOrder();

        $order->complete();
        $order->refund();

        Assert::assertInstanceOf(RefundedState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Can\'t complete');

        $order->complete();
    }
}
