<?php

namespace OOPMentor\OrderState;

use LogicException;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\Attributes\TestDox;

class CancelledStateTest extends AbstractOrderStateTestCase
{
    /**
     * @return void
     */
    #[TestDox("Cancelled order cannot be cancelled again")]
    public function testCancelledOrderCannotBeCancelledAgain(): void
    {
        $order = $this->createOrder();

        Assert::assertEquals('cancelled', $order->cancel());
        Assert::assertInstanceOf(CancelledState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Already cancelled');

        $order->cancel();
    }

    /**
     * @return void
     */
    #[TestDox("Cancelled order cannot be refunded")]
    public function testCancelledOrderCannotBeRefunded(): void
    {
        $order = $this->createOrder();

        Assert::assertEquals('cancelled', $order->cancel());
        Assert::assertInstanceOf(CancelledState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Can\'t refund');

        $order->refund();
    }

    /**
     * @return void
     */
    #[TestDox("Cancelled order cannot be completed")]
    public function testCancelledOrderCannotBeCompleted(): void
    {
        $order = $this->createOrder();

        Assert::assertEquals('cancelled', $order->cancel());
        Assert::assertInstanceOf(CancelledState::class, $order->getState());

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Can\'t complete');

        $order->complete();
    }
}
