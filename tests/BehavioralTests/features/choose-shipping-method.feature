Feature: Choose a shipping method

  Scenario: After paying
    Given I have tangible goods to deliver
    When the user choose a shipping method
    Then the order total should sum the total price of items and the shipping cost
