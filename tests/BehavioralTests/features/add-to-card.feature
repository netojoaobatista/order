Feature: Add an item to the shopping cart

  Scenario: When browsing the catalog in the store
    Given I have at least one item showed to the user
    When the user adds a product to the cart
    Then the shopping cart should have its items increased

  Scenario: User adds multiple items to the cart
    Given I have several items showed to the user
    When the user adds multiple products to the cart
    Then the shopping cart should reflect the total number of items
