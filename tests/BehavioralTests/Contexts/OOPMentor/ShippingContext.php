<?php

namespace BehavioralTests\Contexts\OOPMentor;

use Behat\Behat\Context\Context;
use OOPMentor\Address;
use OOPMentor\Customer;
use OOPMentor\Order;
use OOPMentor\Product;
use OOPMentor\ShippingAddress;
use OOPMentor\ShippingMethod\Delivery;
use PHPUnit\Framework\Assert;

class ShippingContext implements Context
{
    /**
     * @var Order
     */
    private Order $order;

    public function __construct()
    {
        $this->order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );
    }

    /**
     * @Given I have tangible goods to deliver
     */
    public function iHaveTangibleGoodsToDeliver(): void
    {
        $product = new Product('Physical Product', 100);
        $this->order->add($product);
    }

    /**
     * @When the user choose a shipping method
     */
    public function theUserChooseADifferentMethodToShip(): void
    {
        $shippingMethod = new Delivery();
        $this->order->setShippingMethod($shippingMethod);
    }

    /**
     * @Then the order total should sum the total price of items and the shipping cost
     */
    public function theOrderTotalShouldSumTheTotalOfItemsAndTheShippingCost(): void
    {
        Assert::assertEquals(110, $this->order->getTotal());
    }
}
