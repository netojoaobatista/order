<?php

namespace BehavioralTests\Contexts\OOPMentor;

use Behat\Behat\Context\Context;
use OOPMentor\Address;
use OOPMentor\Customer;
use OOPMentor\Order;
use OOPMentor\Product;
use OOPMentor\ShippingAddress;
use OOPMentor\ShippingMethod\Delivery;
use PHPUnit\Framework\Assert;

class ShoppingCartContext implements Context
{
    /**
     * @var Product
     */
    private Product $product;

    /**
     * @var Order
     */
    private Order $order;

    /**
     * @var Product[]
     */
    private array $products = [];

    public function __construct()
    {
        $this->order = new Order(
            new Customer(
                'Neto',
                new Address('123', '456'),
                new ShippingAddress('João', '456', '789')
            )
        );
    }

    /**
     * @Given I have at least one item showed to the user
     */
    public function iHaveAtLeastOneItemShowedToTheUser(): void
    {
        $this->product = new Product('Sample Product', 100);
        Assert::assertNotNull($this->product);
    }

    /**
     * @When the user adds a product to the cart
     */
    public function theUserAddsAProductToTheCart(): void
    {
        $this->order->add($this->product);
    }

    /**
     * @Then the shopping cart should have its items increased
     */
    public function theShoppingCartShouldHaveItsItemsIncreased(): void
    {
        Assert::assertEquals(1, $this->order->count());
    }

    /**
     * @Given I have several items showed to the user
     */
    public function iHaveSeveralItemsShowedToTheUser(): void
    {
        $this->products = [
            new Product('Product 1', 50),
            new Product('Product 2', 30),
            new Product('Product 3', 20),
        ];

        Assert::assertCount(3, $this->products);
    }

    /**
     * @When the user adds multiple products to the cart
     */
    public function theUserAddsMultipleProductsToTheCart(): void
    {
        foreach ($this->products as $product) {
            $this->order->add($product);
        }
    }

    /**
     * @Then the shopping cart should reflect the total number of items
     */
    public function theShoppingCartShouldReflectTheTotalNumberOfItems(): void
    {
        Assert::assertEquals(3, $this->order->count());
    }
}
